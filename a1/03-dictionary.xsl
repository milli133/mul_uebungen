<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/dictionary">
        <html>
            <head>
                <title>Wörterbuch | Statistik</title>

                <!-- Buch-Favicon -->
                <link href="data:image/x-icon;base64,AAABAAEAEBAQAAEABAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAgAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAA7YAMAABkAAD///8AiwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAERERAAAAABEIiIiRAAABCIiIiIiQABCIiIzIiIkAEIzMzMzMyQEIjERMxETIkQiMTEzExMiRCIxETMREyJEIjExMxMTIkQiMREzERMiRCIxMTMTEyJAQjERMxETJABCMzMiMzMkAAQiIiIiIkAAAEQiIiJEAAAAAERERAAAD4HwAA4AcAAMADAACAAQAAgAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIABAACAAQAAwAMAAOAHAAD4HwAA" rel="icon" type="image/x-icon" />

                <!-- Styled with materialized CDN -->
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css" />
            </head>

            <body>
                <div id="hauptcontainer">

                    <h1 class="card-panel green lighten-3 center z-depth-2">Wörterbuch &#128213;</h1>

                    <div>
                        <div id="tabellencontainer" class="card green lighten-5 z-depth-2">
                            <table id="wötertabelle" class="centered striped">
                                <thead>
                                    <tr id="tablerow_header">
                                        <th class="green-text lighten-1">Englisch</th>
                                        <th class="green-text lighten-1">Deutsch</th>
                                        <th class="green-text lighten-1">Kategorie</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <xsl:for-each select="word">
                                        <xsl:sort select="@value" order="descending" />
                                        <tr id="tablerow_content">
                                            <td>
                                                <xsl:value-of select="@value" />
                                            </td>
                                            <td>
                                                <xsl:if test="translation/@lang ='DE'">
                                                    <xsl:value-of select="translation" />
                                                </xsl:if>
                                            </td>
                                            <td id="tabledata_lang">
                                                <xsl:choose>
                                                    <xsl:when test="category='Geography'">
                                                        <xsl:value-of select="category" />
                                                        <span class="emoji">
                                                            &#127757;
                                                        </span>
                                                    </xsl:when>
                                                    <xsl:when test="category = 'Animal'">
                                                        <xsl:value-of select="category" />

                                                        <span class="emoji">
                                                            &#128568;
                                                        </span>
                                                    </xsl:when>
                                                    <xsl:when test="category = 'Food'">
                                                        <xsl:value-of select="category" />
                                                        <span class="emoji">
                                                            &#x1F96A;
                                                        </span>
                                                    </xsl:when>
                                                    <xsl:when test="category = 'Culture'">
                                                        <xsl:value-of select="category" />
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="category" />
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </td>
                                        </tr>
                                    </xsl:for-each>
                                </tbody>
                            </table>
                        </div>

                        <h1 class="card-panel green lighten-3 center z-depth-2">Statistik &#128202;</h1>

                        <div id="statistikcontainer" class="card green lighten-5 z-depth-2">
                            <p class="center"> Die Liste enthält  <xsl:value-of select="count(word)" />
                                Vokabel(n) 
                            </p>
                            <ul class="center">
                                <li>
                                Deutsch:
                                    <xsl:value-of select="count(//translation[@lang='DE'])" />
                                </li>
                                <li>
                                Französisch:
                                    <xsl:value-of select="count(//translation[@lang='FR'])" />
                                </li>
                                <li>
                                Latein:
                                    <xsl:value-of select="count(//translation[@lang='LA'])" />
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </body>
            <style>
                * {
                    margin: 0;
                    padding: 0;
                }

                #hauptcontainer {
                    margin: 20px auto;
                    max-width: 800px;
                }

                #tablecontainer {
                    margin-top: 15px;
                }
                
                #tablerow_header {
                    font-size: 20px; 
                    border-bottom: solid black 2px;
                }

                #tablerow_content {
                    font-size: 16px; 
                    border-bottom: solid lightgrey 1px; 
                    height: 10px;
                }

                #tabledata_lang {
                    font-weight: 600;
                }

                td, th {
                    padding: 10px 5px;
                }

                .emoji {
                    font-size: 25px;
                    color: rgba(0, 0, 0, 0.33);   
                }

                #statistikcontainer {
                    padding: 5px 0; 
                    font-size: 18px;
                }
            </style>
        </html>
    </xsl:template>
</xsl:stylesheet>